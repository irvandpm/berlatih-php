<?php

/* ketentuan 
 jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” 
 jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” 
 jika parameter number lebih besar sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” 
 maka akan mereturn string “Kurang”
*/


function tentukan_nilai($number)
{
    //  kode disini
    if ($number >= 85 && $number <= 100) {
    	echo "Sangat Baik <br>";
    } else if ($number >= 70 && $number < 85) {
    	echo "Baik <br>";
    } else if ($number >= 60 && $number < 70) {
    	echo "Cukup <br>";
    } else {
    	echo "Kurang <br>";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang


// echo "<br>test tambahan <br>";
// echo tentukan_nilai(85);
// echo tentukan_nilai(84);
// echo tentukan_nilai(70);
?>