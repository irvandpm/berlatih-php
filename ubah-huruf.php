<?php
function ubah_huruf($string){
	
	// $abjad = 'abcdefghijklmnopqrstuvwxyz';
	// $posisi = strpos($abjad, 'w');
	// echo "Posisi $posisi <br>";
	// echo "<br>Huruf setelahnya: ".substr($abjad, $posisi+1,1);
	

	for($i=0; $i<strlen($string) ; $i++) {
		$abjad = 'abcdefghijklmnopqrstuvwxyz';
		$posisi = strpos($abjad, substr($string,$i,1),1);
		echo substr($abjad, $posisi+1,1);
	}
}


// TEST CASES

echo ubah_huruf('abc');
echo "<br>";
echo ubah_huruf('wow'); // xpx
echo "<br>";
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br>";
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br>";
echo ubah_huruf('keren'); // lfsfo
echo "<br>";
echo ubah_huruf('semangat'); // tfnbohbu

?>