<?php
function tukar_besar_kecil($string){
	//pisahkan string jadi array

	$pisah = str_split($string);
	// print_r($pisah);
	// echo "<br>";

	foreach ($pisah as $testcase) {
		//cek apakah ada huruf besar?
		if (ctype_upper($testcase)) {
			//kalau ada,kecilkan
			echo strtolower($testcase);
		}	else {
			//kalau gk ada,besarkan
			echo strtoupper($testcase);
		}
	}

}

// echo tukar_besar_kecil ('hELLO');


// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo "<br>";
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo "<br>";
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo "<br>";
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo "<br>";
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"
echo "<br>";

?>